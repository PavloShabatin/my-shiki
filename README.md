# My Shiki

---

## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

## Naming rules

Here are some rules about commit and branches naming that we follow on our project.

1. Your branch name or commit message should start with eiter `feature` or `fix`. Depends on the type of work
2. Then, you should specifi story number after symbol `/`. For example, your branch and it's commit messages should start like this `feature/JIR-123`
3. For commit messagesm, you also shoud specifi type of work after symbol `:`. For example: `feature/JIR-123:test`. Only after that you can write commit message
